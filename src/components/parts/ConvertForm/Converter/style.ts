import styled from 'styled-components'
import { breakpoints } from 'src/components/global/style'

export const CurrencyWrap = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 580px;
  justify-content: center;
  flex-wrap: wrap;
  margin: auto;
  height: 480px;
`
export const Result = styled.span`
  width: 100%;
  padding: 20px 5px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media ${breakpoints.tablet} {
    padding: 20px 35px;
  }
`

export const Currency = styled.span`
  width: 45px;
  font-size: 0.9em;
  font-weight: 500;
  text-align: center;
  padding-left: 10px;
`

export const BadgeWrap = styled.span`
  width: 85px;
  display: flex;
  align-items: center;
  justify-content: left;
`

export const Value = styled.span`
  width: 390px;
  text-align: left;
  font-weight: 500;
  font-size: 1em;
  color: #3c4a5b;

  @media ${breakpoints.tablet} {
    font-size: 1.3em;
  }
`

export const CurrencyImg = styled.img`
  width: 27px;
`
