import { useEffect, useState } from 'react'

import useDebounce from 'src/hooks/useDebounce'
import { defaultCurrencies } from 'src/constants'
import { Converter } from './Converter'
import useSWR, { mutate } from 'swr'
import fetchTicker from 'src/utils/fetchTicker'

import {
  ConverterBox,
  Form,
  Input,
  SelectWrap,
  Selector,
  Caption,
} from './style'

const ConvertForm = ({ initialData }: { initialData: any }): JSX.Element => {
  const [selectCurrency, setSelectCurrency] = useState('USD')
  const [inputValue, setInputValue] = useState(String)
  const debounceInput = useDebounce(inputValue, 500, true)

  // Fetch the data when relevant
  const { data } = useSWR(selectCurrency, fetchTicker, {
    initialData: selectCurrency === 'USD' ? initialData : null, // initial only works for USD
  })

  // Will fetch on debounced input as side effect
  useEffect(() => {
    mutate(selectCurrency, data, true)
  }, [debounceInput])

  return (
    <ConverterBox>
      <Form>
        <Input
          value={inputValue}
          onChange={(value) => {
            const val = value.target.value
            // input numbers only, max length 14, but allow null
            if (!val.match(/^[a-z\b]/i)) {
              if (val.match(/^[0-9|.\b]+$/) && val.length <= 14) {
                setInputValue(val)
              } else if (val.length <= 1) {
                setInputValue(val)
              }
            }
          }}
        />
        <SelectWrap>
          <Selector
            value={selectCurrency}
            onChange={(value) => {
              setSelectCurrency(value.target.value)
            }}
          >
            {defaultCurrencies.map((currency) => (
              <option key={currency} value={currency}>
                {currency}
              </option>
            ))}
          </Selector>
        </SelectWrap>
      </Form>
      {!debounceInput ? (
        <Caption>Enter an amount to check the rates</Caption>
      ) : (
        <Converter
          selectCurrency={selectCurrency}
          inputValue={Number(debounceInput)}
          data={data}
        />
      )}
    </ConverterBox>
  )
}

export default ConvertForm
