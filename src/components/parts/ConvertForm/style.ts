import { breakpoints } from 'src/components/global/style'

import styled from 'styled-components'

export const ConverterBox = styled.div`
  height: 550px;
`

export const Caption = styled.small`
  font-size: 0.9em;
  letter-spacing: 0px;
  color: #8494a5;
`

export const Form = styled.form`
  display: flex;
  justify-content: flex-end;
  max-width: 580px;
  margin: 25px auto;
`

export const Input = styled.input`
  height: 30px;
  border: none;
  border-radius: 10px;
  font-size: 1.4em;
  padding: 30px 15px;
  background-color: #f5f9fc;
  color: #3c4a5b;
  font-weight: 300;
  width: 100%;
  @media ${breakpoints.tablet} {
    height: 80px;
    font-size: 3em;
    padding: 30px;
  }
`

export const SelectWrap = styled.span`
  position: absolute;
  width: 75px;
  margin: 7px auto;

  @media ${breakpoints.tablet} {
    width: 150px;
    margin: 17px auto;
  }
`

export const Selector = styled.select`
  appearance: none;
  background-image: url('/assets/br_down.webp'),
    url('/assets/${(props) => props.value}.png');
  background-repeat: no-repeat, no-repeat;
  background-size: 10px, 27px;
  background-position: 90px, 12px 10px;
  background-color: #fff;
  border: 0px none;
  font-family: inherit;
  font-size: inherit;
  cursor: inherit;
  line-height: inherit;
  height: 47px;
  width: 50px;
  border-radius: 50px;
  font-size: 0.9em;
  font-weight: 500;
  margin: 0px;
  padding-left: calc(50% - 2.1em);
  -webkit-padding-start: calc(50% - 1.7em);
  outline: none;
  font-size: 0px;

  @media ${breakpoints.tablet} {
    width: 115px;
    font-size: 0.9em;
  }
`
