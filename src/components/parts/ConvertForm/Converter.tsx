import { defaultCurrencies } from 'src/constants'

import {
  BadgeWrap,
  CurrencyImg,
  Currency,
  CurrencyWrap,
  Result,
  Value,
} from './Converter/style'

const CurrencyBadge = ({ currency }: { currency: string }) => (
  <BadgeWrap>
    <CurrencyImg src={`/assets/${currency}.png`} />
    <Currency>{currency}</Currency>
  </BadgeWrap>
)

export const Converter = ({
  selectCurrency,
  inputValue,
  data,
}: {
  selectCurrency: string
  inputValue: number
  data: any
}): JSX.Element => {
  if (!data) return <></> // data loading, could put a spinner here

  // Do not include currently selected currency
  const currencies = defaultCurrencies.filter((i) => i != selectCurrency)

  // Filter the data for the currencies we want
  const matchCurrencies = currencies.map((currency) =>
    data.find((ticker: { pair: string }) => ticker.pair.startsWith(currency))
  )

  // Calculate input, add commas, handle small numbers
  const calculateValue = (value: number): string => {
    const amount = inputValue / Number(value) || 0 // NaN protection
    return amount > 1
      ? amount.toLocaleString() // add the commas
      : amount.toFixed(5) // too small of a number
  }

  return (
    <CurrencyWrap>
      {matchCurrencies.map((i) => (
        <Result key={i.pair}>
          <Value>{calculateValue(i.ask)}</Value>
          <CurrencyBadge
            currency={i.pair.split(i.currency)[0].replace('-', '')}
          />
        </Result>
      ))}
    </CurrencyWrap>
  )
}

export default Converter
