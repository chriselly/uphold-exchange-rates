import styled from 'styled-components'

import { breakpoints } from 'src/components/global/style'

export const Headline = styled.h2`
  font-size: 1.5em;
  font-weight: 600;

  @media ${breakpoints.tablet} {
    font-size: 2.6em;
  }
`

export const Subtitle = styled.p`
  font-size: 1em;
  max-width: 500px;
  font-weight: 300;
  color: #68778d;
  margin: auto;
  line-height: 1.6em;
  margin-bottom: 40px;

  @media ${breakpoints.tablet} {
    font-size: 1.2em;
  }
`
