import ConvertForm from '../../parts/ConvertForm'

import { Container, GlobalStyle } from 'src/components/global/style'

import { Headline, Subtitle } from './style'

const ExchangeRates = ({ initialData }: { initialData: any }): JSX.Element => {
  return (
    <>
      <Container>
        <GlobalStyle />
        <Headline>Currency Converter</Headline>
        <Subtitle>
          Receive competitive and transparent pricing with no hidden spreads.
          See how we compare.
        </Subtitle>
        <ConvertForm initialData={initialData} />
      </Container>
    </>
  )
}

export default ExchangeRates
