import styled, { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  * {
  box-sizing: border-box;
  word-wrap: break-word;
  }
  *:focus {
      outline: none;
  }
  body {
    font-family: "Montserrat", Arial, Helvetica, Verdana, sans-serif;
    font-size: 16px;
    font-weight: normal;
    letter-spacing: .03rem;
    margin: 0 auto;
  }
  h1 {
    font-size: 4rem;
  }
  a {
    color: #8494a5;
    text-decoration: none;
    cursor: pointer;
  }
  a:hover {
    text-decoration: underline;
  }
  img {
    border: 0px;
    width: 100%;
  }
  
`

export const Container = styled.div`
  max-width: 930px;
  margin: 0 auto;
  overflow: hidden;
  text-align: center;
  padding: 60px 20px;
`

export const breakpoints = {
  tablet: '(min-width: 564px)',
  desktop: '(min-width: 768px)',
}
