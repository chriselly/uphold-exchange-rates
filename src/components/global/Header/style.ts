import styled from 'styled-components'
import { breakpoints } from 'src/components/global/style'

export const HeaderWrap = styled.header`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  display: flex;
  margin: auto;
  height: 100px;
  max-width: 1200px;
  font-size: 0.9em;
  letter-spacing: 0;
  padding: 0px 20px;
  @media ${breakpoints.desktop} {
    height: 160px;
  }
`

export const MobileOnly = styled.div`
  height: 30px;
  display: block;

  @media ${breakpoints.desktop} {
    display: none;
  }
`
export const Hamburger = styled.span`
  display: block;
  width: 33px;
  height: 4px;
  margin-bottom: 5px;
  position: relative;

  background: #cdcdcd;
  border-radius: 3px;

  z-index: 1;

  transform-origin: 4px 0px;

  transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1),
    background 0.5s cubic-bezier(0.77, 0.2, 0.05, 1), opacity 0.55s ease;
`

export const LogoWrap = styled.div`
  text-align: center;
  width: 100%;
  height: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;

  @media ${breakpoints.desktop} {
    height: 160px;
  }
`

export const Logo = styled.img`
  width: 128px;
`

export const Actions = styled.div`
  a {
    padding-right: 20px;
  }
  display: none;

  @media ${breakpoints.desktop} {
    display: block;
  }
`

export const Login = styled.div`
  text-align: right;
  display: none;

  @media ${breakpoints.desktop} {
    display: block;
  }
`
