import { GlobalStyle } from 'src/components/global/style'

import {
  LogoWrap,
  Logo,
  HeaderWrap,
  MobileOnly,
  Hamburger,
  Actions,
  Login,
} from './style'

const Header = (): JSX.Element => {
  return (
    <>
      <GlobalStyle />
      <LogoWrap>
        <a href="#">
          <Logo src={'assets/logo.svg'} />
        </a>
      </LogoWrap>
      <HeaderWrap>
        <MobileOnly>
          <Hamburger />
          <Hamburger />
          <Hamburger />
        </MobileOnly>
        <Actions>
          <a href="#">Personal</a>
          <a href="#">Business</a>
          <a href="#">Partners</a>
        </Actions>
        <Login>
          <a href="#">Log In</a>
        </Login>
      </HeaderWrap>
    </>
  )
}

export default Header
