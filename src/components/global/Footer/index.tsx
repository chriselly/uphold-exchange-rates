import { GlobalStyle } from 'src/components/global/style'

import {
  FooterWrap,
  NavWrap,
  LogoWrap,
  Section,
  Logo,
  SectionHeader,
  SectionItem,
  Copyright,
  AppSection,
  AppStore,
} from './style'

const Footer = (): JSX.Element => {
  return (
    <>
      <GlobalStyle />
      <FooterWrap>
        <NavWrap>
          <LogoWrap>
            <Logo src={'assets/logo.svg'} />
          </LogoWrap>
          <Section>
            <SectionHeader>Products</SectionHeader>
            <SectionItem>Consumers</SectionItem>
            <SectionItem>Business</SectionItem>
            <SectionItem>Partners</SectionItem>
          </Section>
          <Section>
            <SectionHeader>Company</SectionHeader>
            <SectionItem>About</SectionItem>
            <SectionItem>Carrers</SectionItem>
            <SectionItem>Press</SectionItem>
            <SectionItem>Blog</SectionItem>
          </Section>
          <Section>
            <SectionHeader>Help</SectionHeader>
            <SectionItem>FAQ &amp; Support</SectionItem>
            <SectionItem>Platform Status</SectionItem>
            <SectionItem>Criptionary</SectionItem>
            <SectionItem>Pricing</SectionItem>
            <SectionItem>Legal</SectionItem>
          </Section>
          <Section>
            <SectionHeader>Social</SectionHeader>
            <SectionItem>Facebook</SectionItem>
            <SectionItem>Twitter</SectionItem>
            <SectionItem>Instagram</SectionItem>
            <SectionItem>LinkedIn</SectionItem>
          </Section>
          <AppSection>
            <AppStore src={'assets/appstore.svg'} />
            <AppStore src={'assets/playstore.svg'} />
          </AppSection>
        </NavWrap>
        <Copyright>
          Uphold Europe Limited, Reg No. 09281410, Registered Office:
          Interchange Triangle, Chalk Farm Road, London, England, NW1 8AB
          <br />
          &copy; Uphold, Inc. 2018. All Rights Reserved. Agreements Privacy
          &amp; Data Policy Cookie Policy
        </Copyright>
      </FooterWrap>
    </>
  )
}

export default Footer
