import styled from 'styled-components'
import { breakpoints } from 'src/components/global/style'

export const FooterWrap = styled.footer`
  margin: auto;
  max-width: 930px;
  padding: 0px 20px;
  font-size: 0.9em;
  letter-spacing: 0;
  color: #3c4a5b;
  line-height: 2em;
  border-top: 1px solid #8494a530;
  @media ${breakpoints.desktop} {
    padding: 60px 20px;
    border-top: 0px;
  }
`

export const NavWrap = styled.div`
  flex-direction: row;
  display: flex;
  margin: auto;
  padding-bottom: 30px;
  padding-top: 60px;
  border-top: 0px none;

  flex-wrap: wrap;
  width: 100px;
  justify-content: left;

  @media ${breakpoints.desktop} {
    padding-top: 90px;
    flex-wrap: nowrap;
    justify-content: space-between;
    width: 100%;
    border-top: 1px solid #8494a530;
  }
`

export const LogoWrap = styled.div`
  padding-right: 60px;
  padding-bottom: 20px;
`
export const Logo = styled.img`
  width: 90px;
`

export const Section = styled.div`
  padding: 0px 20px;
  padding-bottom: 40px;
`

export const SectionHeader = styled.div`
  font-weight: 500;
  padding-bottom: 20px;
`

export const SectionItem = styled.div`
  padding: 0px;
`

export const AppSection = styled.div`
  padding-left: 9px;

  @media ${breakpoints.desktop} {
    padding-left: 90px;
  }
`

export const AppStore = styled.img`
  width: 20px;
  margin: 0px 10px;
`

export const Copyright = styled.div`
  padding-bottom: 20px;
  color: #8494a5;
  font-size: 0.75em;
  line-height: 2.3em;
  font-weight: 300;
  opacity: 0.7;
  letter-spacing: 0.2px;
  text-align: center;

  @media ${breakpoints.desktop} {
    text-align: left;
  }
`
