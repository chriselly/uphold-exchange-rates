import { NextApiRequest, NextApiResponse } from 'next'
import { upholdTicker } from 'src/utils/upholdTicker'

export default async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  const { currency } = req.body

  return new Promise<void>((resolve) => {
    upholdTicker(currency)
      .then((data) => {
        res.setHeader('Content-Type', 'application/json')
        res.setHeader('Cache-Control', 'max-age=180000')
        res.status(200).json(data)
        resolve()
      })
      .catch((error) => {
        res.json(error)
        res.status(405).end()
        return resolve()
      })
  })
}
