import Head from 'next/head'
import { GoogleFonts } from 'next-google-fonts'
import Header from 'src/components/global/Header'
import Footer from 'src/components/global/Footer'
import ExchangeRates from 'src/components/pages/ExchangeRates'
import PropTypes from 'prop-types'
import { upholdTicker } from 'src/utils/upholdTicker'

export default function HomePage({
  initialData,
}: {
  initialData: any
}): JSX.Element {
  return (
    <>
      <GoogleFonts href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" />
      <Head>
        <title>Currency Converter - Uphold</title>
      </Head>
      <Header />
      <ExchangeRates initialData={initialData} />
      <Footer />
    </>
  )
}

HomePage.getInitialProps = async () => {
  const data = await upholdTicker('USD')
  return { initialData: data }
}

HomePage.propTypes = {
  initialData: PropTypes.any,
}
