import getConfig from 'next/config'
import SDK from '@uphold/uphold-sdk-javascript'

export const upholdTicker = (currency: string): Promise<void> => {
  const { serverRuntimeConfig } = getConfig()
  const sdk = new SDK({
    baseUrl: serverRuntimeConfig.apiUrl,
    clientId: serverRuntimeConfig.clientId,
    clientSecret: serverRuntimeConfig.clientSecret,
  })

  return new Promise<void>((resolve, reject) => {
    return sdk
      .getTicker(currency)
      .then((data) => {
        return resolve(data)
      })
      .catch((error) => {
        return reject(error)
      })
  })
}
