import fetch from 'isomorphic-unfetch'

export const fetchTicker = async (currency = 'USD'): Promise<void> => {
  return fetch('/api/ticker', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ currency: currency }),
  }).then((r) => {
    return r.json()
  })
}

export default fetchTicker
