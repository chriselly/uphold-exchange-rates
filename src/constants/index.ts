// You can change the currencies here for the entire site

export const defaultCurrencies = [
  'USD',
  'EUR',
  'BAT',
  'BTC',
  'BCH',
  'CNY',
  'ETH',
  'GBP',
]
