# uphold-exchange-rates

- Convert exchange currencies using the Uphold API

### Getting started

```
npm install
npm run dev

# or

yarn
yarn dev
```
