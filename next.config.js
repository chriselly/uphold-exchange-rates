module.exports = {
  serverRuntimeConfig: {
    apiUrl: process.env.API_HOST,
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
  },
}
